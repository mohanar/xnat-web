<script type="text/javascript" src="$content.getURI("scripts/project/accessibility.js")"></script>

$page.setTitle("XDAT")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)
#set($months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"])
#set($days = [ 1..31 ])
#set($years = [ $!turbineUtils.getYear()..1900])
#if ($data.message)
<div class="alert">$data.message</div>
#end

<form id="new-project-form" name="form1" method="post" action="$link.setAction("AddProject")" ONSUBMIT="return validateProjectForm(this);">

    #if($vr)
        <div class="error">Invalid parameters:<BR>$vr.toHTML()</div>
        <HR>
    #end

    <DIV class="edit_title">$page_title</DIV>

    <DIV class="container">
        <DIV class="withColor containerTitle" style="font-weight:700;">&nbsp;Step 1: Enter $displayManager.getSingularDisplayNameForProject().toLowerCase() details</DIV>
        <DIV class="containerBody">
            <DIV class="containerItem">
                #set( $parentTemplate = 'add' )
                #parse("/screens/xnat_projectData/edit/details.vm")
            </DIV>
        </DIV>
    </DIV>

    <br>

    <div class="container">
        <div class="withColor containerTitle">&nbsp;Step 2: Define $displayManager.getSingularDisplayNameForProject() Accessibility</div>
        <div class="containerBody">
            <div class="containerItem">
                Select the accessibility of your $displayManager.getSingularDisplayNameForProject().toLowerCase().
            </div>
            <hr class="h20">
            <div class="containerItem">
                <table width="100%">
                    <tr>
                        <td class="highlighted" valign="top">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="top" style="padding:10px 20px 10px 10px;border-right:1px solid #ccc;">
                                        <label id="private_access_div" title="Only you and study members will be able to access study data." style="display:block;white-space:nowrap;padding-bottom:5px;" onclick="checkAccessibilityRadioButton(this, 'private_access');">
                                            <input type="radio" id="private_access" name="accessibility" value="private" checked/> Private
                                        </label>

                                        <label id="protected_access_div" title="All users will be able to see your study title and description, but only collaborators you approve will be able to analyze and download data." style="display:block;white-space:nowrap;padding-bottom:5px;" onclick="checkAccessibilityRadioButton(this, 'protected_access');">
                                            <input type="radio" id="protected_access" name="accessibility" value="protected"/> Protected
                                        </label>

                                        <label id="public_access_div" title="All users will be able to access study data for analysis and download." style="display:block;white-space:nowrap;" onclick="checkAccessibilityRadioButton(this, 'public_access');">
                                            <input type="radio" id="public_access" name="accessibility" value="public"/> Public
                                        </label>
                                    </td>
                                    <td valign="top" style="padding:10px 160px 10px 20px;">
                                        <div id="balloon" style="border:none;">Only you and study members will be able to access study data.</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br/>
            </div>
        </div>
    </div>

    #xdatEditProps($item $edit_screen)

    <div class="submit-right">
        <input class="btn1" type="submit" name="eventSubmit_doPerform" value="Create Project">
        <div class="clear"></div>
    </div>

</form>

<SCRIPT type="text/javascript">

    function validateProjectForm(_form) {

        var __form = jq(_form);
        __form.find('.invalid').removeClass('invalid');

        var Project = XNAT.app.displayNames.singular.project;
        var __projID = jq(document.getElementById("xnat:projectData/ID"));
        var __projTitle = jq(document.getElementById("xnat:projectData/name"));
        var __projRunningTitle = jq(document.getElementById("xnat:projectData/secondary_ID"));

        var projIDVal = jQuery.trim(__projID.val());
        var projTitleVal = jQuery.trim(__projTitle.val());
        var projRunningTitleVal = jQuery.trim(__projRunningTitle.val());

        var invalid_count = 0;
        if (projIDVal == '') {
            xmodal.message('Validation', 'Please enter a ' + Project.toLowerCase() + ' abbreviation (ID).');
            __projID.addClass('invalid');
            invalid_count++;
        }
        if (projRunningTitleVal == '') {
            xmodal.message('Validation', 'Please enter a running title.');
            __projRunningTitle.addClass('invalid');
            invalid_count++;
        }
        if (projRunningTitleVal.length > 24) {
            xmodal.message('Validation', 'Please enter a running title of 24 characters or less.');
            __projRunningTitle.addClass('invalid');
            invalid_count++;
        }
        if (projTitleVal == '') {
            xmodal.message('Validation', 'Please enter a ' + Project.toLowerCase() + ' title.');
            __projTitle.addClass('invalid');
            invalid_count++;
        }
        if (projTitleVal.length > 199) {
            xmodal.message('Validation', 'Please enter a ' + Project.toLowerCase() + ' title of 199 characters or less.');
            __projTitle.addClass('invalid');
            invalid_count++;
        }
        if (invalid_count > 0) {
            __form.find('.invalid').first().focus();
            return false;
        }
        else {
            __projID.val(projIDVal);
            __projTitle.val(projTitleVal);
            __projRunningTitle.val(projRunningTitleVal);
            return true;
        }
    }

</SCRIPT>
